import numpy as np

a = np.array([0,1,2,3,4,5])
print(a)
print(a.dtype)   # dtype('int32')
print(a.size)     # 6
print(a.ndim)     # 1
print(a.shape)    # (6,)
print(a.itemsize) # 4 bytes
print(a.nbytes)   # 24

b = np.array([[1,2,3,4], [4,5,6,7], [7,8,9,10.]])
print(b)
print(b.dtype)    # float64
print(b.size)     # 12
print(b.ndim)     # 2
print(b.shape)    # (3, 4)
print(b.itemsize) # 8
print(b.nbytes)   # 12*8=96

# 資料型別轉換
b.astype('int32')
print(b.dtype)
b = b.astype('int32')
print(b.dtype)    # int32

# 實作練習
# 建立3維陣列 myzero, 5個元素, 每個元素為3列,4行的零矩陣
# myzero.shape 結果為 (5, 3, 4)

#z1 = np.array()
#z2 = np.array(3)
myzero = np.array(5, 3, 4, ndmin=3)
print(myzero.shape)