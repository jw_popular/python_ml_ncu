##############################
# 主成分分析 Principal Component Analysis, PCA
##############################

# find covariance matrix, eigenvalue and eigenvector

# Covariance is a measure of how changes in one variable are associated with changes 
# in a second variable. Specifically, it’s a measure of the degree to which two 
# variables are linearly associated.

import numpy as np

x1 = [90, 88, 60, 73, 82, 69, 77, 88, 59, 72]
x2 = [280, 261, 183, 277, 230, 234, 263, 278, 197, 230]

data = np.array([x1, x2])
print(data)

cov_matrix = np.cov(data)
print(cov_matrix)
# array([[ 126.62222222,  316.84444444],
#        [ 316.84444444, 1178.67777778]])

# x1 variance : 126.62
# x2 variance : 1178.68

eigen_vals, eigen_vecs = np.linalg.eig(cov_matrix)

print("Eigenvalues: ", eigen_vals)
# lambda_1 = 1266.73112247, lambda_2 = 38.56887753

print("Eigenvectors as columns: ", eigen_vecs)