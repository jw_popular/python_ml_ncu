##############################
# PCA demo - iris
##############################

# 載入模組
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt

import numpy as np


# 載入 iris 資料集
iris = load_iris()
print(iris)

# 區分特徵與反應變數
iris_X, iris_y = iris.data, iris.target

print(iris_X)

print(iris_y)

# 特徵名稱 (X)
print(iris.feature_names)

# 反應變數(Y)的三種水準
print(iris.target_names)

# 因子化反應變數 {0: 'setosa', 1: 'versicolor', 2: 'virginica'}
label_dict = {i: k for i, k in enumerate(iris.target_names)}

print(label_dict)

# 繪製散佈圖
def plot(X, y, title, x_label, y_label):
    for label,marker,color in zip(range(3), 
                                  ('^', 's', 'o'), 
                                  ('blue', 'red', 'green')):
        plt.scatter(x=X[:,0].real[y == label],
                    y=X[:,1].real[y == label],
                    color=color,
                    alpha=0.5,
                    label=label_dict[label])
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        leg = plt.legend(loc='upper right')
        leg.get_frame().set_alpha(0.5)
        plt.title(title)
    
plot(iris_X, iris_y, "Original Iris Data", "sepal length (cm)", "sepal width (cm)")

plt.show()

# 計算各行平均值向量
mean_vector = iris_X.mean(axis=0)
print(mean_vector)

# 計算共變異數矩陣
# var(x) = sum(x[i] - x_bar)^2)/(n-1)
# cov(x,y) = sum((x[i] - x_bar)*(y[i] - y_bar))/(n-1)

cov_mat = np.cov((iris_X).T)

print(cov_mat)

print("cov_mat shape = ", cov_mat.shape) # 4*4

##############################
# 步驟2 共變異數矩陣的特徵值計算
##############################

# 考慮非零向量 x, 如果 Ax = λx, 則稱 x 為 A 的特徵向量, λ 為 A 的特徵值.
# Ax - λIx = 0, 其中 I 為單位矩陣
# (A - λI)x = 0
# (A - λI) = 0, 即 (A - λI) 必為奇異矩陣 (singular matrix).
# det(A - λI) = 0, 可以解出 λ, 並進而解出 A

eig_val_cov, eig_vec_cov = np.linalg.eig(cov_mat)

# 特徵值, 計算結果為4個
print("eig_val_cov = ", eig_val_cov)

# 特徵向量
print("eig_vec_cov = ", eig_vec_cov)

# 依遞減排序顯示
for i in range(len(eig_val_cov)):
    eigvec_cov = eig_vec_cov[:,i]
    print('特徵向量 {}: \n{}'.format(i+1, eigvec_cov))
    print('特徵值 {} Covariance matrix: {}'.format(i+1, eig_val_cov[i]))
    print(30 * '-')

##############################
# 步驟3 保留前 k 個特徵值- scree plot
##############################

# 使用陡坡圖 (碎石圖) Scree plot 決定保留 k 個特徵值
# 保留 k 個特徵值, 即是決定主成分個數, 所有主成分解釋100%資料的總變異數.
# 主成分(Principal Component)解釋變異數百分比 = "將每個主成分的特徵值"除以"所有特徵值的總和"
explained_variance_ratio = eig_val_cov/eig_val_cov.sum()

print("explained_variance_ratio = ", explained_variance_ratio)
# array([0.92461872, 0.05306648, 0.01710261, 0.00521218])

# 累計主成分解釋變異數百分比
print("**** cumsum ==> ", np.cumsum(explained_variance_ratio))

# 陡坡圖
plt.plot(np.cumsum(explained_variance_ratio), 
         linestyle='--', 
         marker='o', 
         color='b')
# plt.plot(np.cumsum(explained_variance_ratio), '--bo') # 與上面相同
plt.title('iris - Scree Plot')
plt.xlabel('Principal Component (k)')
plt.ylabel('% of Variance Explained <= k')

plt.show()

# 結論: 使用前2個主成分已經解釋原始資料變異數的98%, 達成維度縮減之目的.

