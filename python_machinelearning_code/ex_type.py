# 整數 int
x1 = 1
print(type(x1))
print(float(x1))

# 浮點數 float
x2 = 1.234
x2_1 = 1.366
x2_2 = 1.500000000000
print(type(x2))
print(x2+x2_1)
print("%08.3f"%x2_2)

# 複數  complex
x3 = 1+2j
x3_1 = 1+2j
print(type(x3))
print(x3)
print(x3*5)
print(x3+x3_1)

# 布林值 (Boolean)
x4 = True
print(type(x4))
print(x4 + 10)
print(x4*5)


x5 = 1.234e3
x6 = 1.234e-3
print(x5)
print(x6)

x7 = 1e3
x8 = 1e-3
print(x7*x8)