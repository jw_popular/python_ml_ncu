"""
File     : PYTHON機器學習應用班.py
Date     : 2022/09/24,10/01
Author   : Ming-Chang Lee
Email    : alan9956@gmail.com
RWEPA    : http://rwepa.blogspot.tw/
GitHub   : https://github.com/rwepa
Encoding : UTF-8
"""

# 課程大綱

# 第一天
# 1.機器學習簡介
# 2.CRISP-DM標準流程與模型績效評估
# 3.案例實作練習
# 4.關聯規則
# 5.PCA與集群分析
# 6.案例實作練習

# 第二天
# 7.迴歸分析
# 8.決策樹與隨機森林法
# 9.案例實作練習
# 10.時間序列分析
# 11.類神經網路與深度學習
# 12.案例實作練習

##############################
# 1.機器學習簡介
##############################

# 個人簡介 http://rwepa.blogspot.com/

# Python 程式設計-李明昌 免費電子書
# http://rwepa.blogspot.com/2020/02/pythonprogramminglee.html

# APC方法
# 1.群組
# 2.時間
# 3.建立評估變數

# 資料分析/視覺化應用

# 2020新型冠狀病毒視覺化
# http://rwepa.blogspot.com/2020/02/2019nCoV.html

# shiny 顧客連接分析
# https://rwepa.shinyapps.io/shinyCustomerConnect/

# 品質管制圖(quality control chart)應用
# http://rwepa.blogspot.com/2021/10/r-shiny-quality-control-chart.html

# https://github.com/rwepa/DataDemo/blob/master/spc_wafer_with_header.csv

# https://github.com/rwepa/DataDemo/blob/master/spc_pistonrings_without_header.csv

# Taiwan Stock App
# https://rwepa.shinyapps.io/shinyStockVis/

# rwepa-arduino系列-3 python + tkinter - LED 應用
# https://youtu.be/LjgFIm1S7tw

##############################
# Anaconda 下載
##############################

# https://www.anaconda.com/

# 實作練習
# 在 jupyter notebook 輸入以下程式碼練習
from numpy import *
random.rand(5,5)

# jupyter-notebook 更改預設目錄
# 程式集 \ Windows 系統 \ 命令提示字元
# cd C:\
# jupyter-notebook

# Jupyter Notebook 快速鍵
# 按 [Esc] cell旁邊為藍色
# 按 x：刪除當前選擇的cell
# 按 a：在當前選擇的上方新增一個cell
# 按 b：在當前選擇的下方新增一個cell
# 按 Shift + Enter：執行當前的cell並且選到下一個cell
# 按 Ctrl + Enter：執行當前cell
# 按 M：轉成markerdown模式，可以看到紅色框框內容從code變成markerdown

# 開啟 Python程式設計-李明昌.ipynb --> 加入數學式
# http://rwepa.blogspot.com/2020/02/pythonprogramminglee.html
# 參考 Python_Programming_Lee.pdf 第14頁
# Python_Programming_Lee_ipynb.zip 解壓縮
# 開啟 Python程式設計-李明昌.ipynb

# Jupyter Notebook 加入數學式
# 方法1: 將LaTeX代碼括在美元符號 $ ... $中，表示在文字中顯示數學式。

# 安裝 Orange
# conda install -c conda-forge orange3

# 使用命令提示列 開啟 Orange
# python -m Orange.canvas

# Python Orange - 關聯規則教學
# https://youtu.be/rh5GxJamtNg
# http://rwepa.blogspot.com/2022/07/python-orange-associate-tutorial.html

# 已安裝模組(pip)
# pip list
# python -c help('os')

##############################
# Anaconda 模組管理
##############################

# 顯示已安裝模組
# conda list

# 尋找官網套件
# conda search matplotlib

# 安裝模組
# conda install 模組名稱

# 更新模組
# conda update 模組名稱

# 移除模組
# conda remove 模組名稱

# https://github.com/rwepa/DataDemo/blob/master/iPAS-python-program.py#L52

# conda 虛擬環境

# 檢視所有虛擬環境清單 	conda env list
# 啟用 myenv 虛擬環境 	conda activate myenv
# 關閉虛擬環境 			conda deactivate
# 建立 myenv 虛擬環境 	conda create --name myenv
# 建立特定 python 版本的虛擬環境
# conda create -n myenv python=3.9
# 建立特定 scipy 模組版本的虛擬環境
# conda create -n myenv scipy=1.9.0

# 參考 https://github.com/rwepa/DataDemo/blob/10c8ab50a0871a6d30b212cef3fc865248532a39/iPAS-python-program.py#L2658

##############################
# Spyder 軟體簡介
##############################

# Spyder 更新

# 步驟1.Anaconda 整體更新
# conda update anaconda

# 步驟2.Spyder 安裝
# conda install spyder

# 步驟2a.Spyder 更新
# conda update spyder

"""
# RStudio – 執行 Python 
# 2022.9.6
# Click [Terminal]
# conda env list
# conda activate base
# python

# Ctrl + Alt + Enter
import pandas as pd
print(pd.__version__)
"""

##############################
# 變數
##############################

# 合法變數
CustomerSaleReport = 1
_CustomerSaleReport = 1
Customer_Sale_Report = 1
customer_sale_report = 1
大數據 = 1 # 中文亦可,建議不要使用

# 不合法變數
$CustomerSaleReport = 1 # SyntaxError: invalid syntax
2020_sale = 100 # SyntaxError: invalid decimal literal
break = 123 # SyntaxError: invalid syntax

# 內建保留字
dir(__builtins__)
len(dir(__builtins__)) # 160

# Python 註解
# 使用一個 #	 用於1行註解
# 使用二個 """  用於超過1行註解或函數之說明文件

# 內縮4個空白鍵之語法

##############################
# 資料型別與運算子
##############################

# 資料型別(資料型態)
# https://docs.python.org/3/library/stdtypes.html

# 資料型別-範例

# 整數 int
x1 = 1
type(x1)

# 浮點數 float
x2 = 1.234
type(x2)

# 複數  complex
x3 = 1+2j
type(x3)

# 布林值 (Boolean)
x4 = True
type(x4)
x4 + 10

# None值
import numpy as np
None == False
None == 0
False == 0
True == 1
None == np.nan
None == None

# 整數亂數
# https://docs.python.org/3/library/random.html
# randrange(start, stop[, step])
# 包括 start, 不包括stop
import random
random.seed(168)
myrandom = random.randrange(1, 100)
myrandom

# 運算子
3 + 5
3 + (5 * 4)
3 ** 2
"Hello" + "World"
1 + 1.234
7 / 2
7 // 2
7 % 2
2 ** 10
1.234e3 - 1000

x5 = 1 == 2
x5
x5 + 10

# 指派運算子
x = 9
x+=2  # 相同於 x = x+2 = 9+2=11
print(x)

##############################
# 資料物件
##############################

# 資料物件
# Tuple 序列 (元組) - (value,...) 不可修改
# List 串列(清單)   - [value,...]
# Set 集合          - {value,...}
# Dict 字典         - {key:value,...}

##############################
# Tuple 序列 (元組)
##############################

# 建立序列
f = (2,3,4,5) # A tuple of integers
g = () # An emptmy tuple
h = (2, [3,4], (10,11,12)) 	# A tuple containing mixed objects

# Tuples操作
x = f[1] # Element access. x = 3
x

y = f[1:3] # Slices. y = (3,4)
y

z = h[1][1] 	# Nesting. z = 4
z

# tuple - loop 處理
fruits = ("apple", "banana", "cherry")

# 方法1. tuple - 取出元素, 使用for
for x in fruits:
  print(x)

# 方法2. tuple - 取出元素, 使用while
i = 0
while i < len(fruits):
  print(fruits[i])
  i = i + 1
  
# 方法3. tuple - 取出元素, 使用指標 range, len
for i in range(len(fruits)):
  print(fruits[i])

##############################
# List 串列(清單)
##############################

# 建立串列
a = [2, 3, 4]            # 整數串列
b = [2, 7, 3.5, "Hello"] # 混合資料串列
c = []	                 # 空串列
d = [2, [a, b]]	         # 巢狀串列

# 串列的操作
a
a[1] 	   # 取得第2個元素
a[-1]      # 取得最後一個元素
b[1:3] 	   # 串列篩選
d[1][0][2] # 巢狀串列操作
b[0]       # 2
b[0] = 42  # 修改元素值
b[0]       # 42

# 串列長度
len(b)

##############################
# Set 集合
##############################

# 集合與字典相似, 但字典沒有key,只有值
# 集合內容不可以修改
# 集合是  unordered
# 集合是  unindexed
# 集合會忽略重複的值

a = set() # 空集合
type(a)

b = {"台北市", "新北市", "桃園市", "台中市", "台北市", "新北市", "高雄市"}
b # {'台中市', '台北市', '新北市', '桃園市', '高雄市'}

# b[0] = 1 # TypeError: 'set' object does not support item assignment
# b[0]     # TypeError: 'set' object is not subscriptable

len(b)

##############################
# Dict 字典
##############################

# 字典宣告
mydict = {
    "language": "Python",
    "designer": "Guido van Rossum",
    "year": 1991
    }

print(mydict)

type(mydict) # dict

# 重複 key, 只保留1個
mydict1 = {
    "language": "Python",
    "designer": "Guido van Rossum",
    "year": 1991,
    "year": 2021
    }

print(mydict1)

# 字典存取元素 – keys, values
b = {
     "uid": 168, 
     "login": "marvelous", 
     "name" : 'Alan Lee'
     }
b

# dict 取得所有 keys
mykeys = b.keys()
print(mykeys)

# dict 取得所有 values
myvalues = b.values()
print(myvalues)

# dict 取得key的值
u = b["uid"] # 168
print(u)

# dict 更新值
b.update({"uid": 123})
print(b)

# dict 新增元素
b["shell"] = "/bin/sh"
print(b)

##############################
# 模組 (Modules) 套件, 包
##############################

# 使用模組
import math
math.sqrt(9)

from math import sqrt
sqrt(9)

##############################
os 模組
##############################

# 切換工作目錄
import os
os.getcwd() # 讀取工作目錄
os.chdir("C:/") # 變更工作目錄
os.getcwd()
os.listdir(os.getcwd()) # 顯示檔案清單

# 模組的搜尋路徑
import sys
sys.path
# '' 表示現行目錄

# 實作練習
# 自訂模組, 計算商數與餘數
# 自訂模組檔案名稱為 numberscompute.py

# numberscompute.py
def divide(a,b):
	q = a/b        	# q 商數
	r = a - q*b  	# r 餘數
	return q,r

# 練習檔案名稱為 mycompute.py
import numberscompute
x,y = numberscompute.divide(42,5)

##############################
# 機器學習
##############################

# 監督式學習 Supervised learning - 執行 X --> 預測 --> Y
# 迴歸分析 Regression analysis
# 廣義線性模型 General linear model (GLM)
# 天真貝氏法 Naïve-Bayes
# K近鄰法 k-nearest neighbors (KNN)
# 決策樹 Decision tree, 隨機森林法 Random Forest
# 支持向量機 Support vector machine (SVM)
# 類神經網路 Neural network (NN)
# 集成學習 Ensemble learning

# 非監式督學習 Unsupervised learning
# 集群法 Clustering
# 關聯規則 Association rule
# 主成分分析 Principal Component Analysis

##############################
# scikit-learn 簡介
##############################

import sklearn
print("The scikit-learn version is", format(sklearn.__version__))

# Choosing the right estimator
# https://scikit-learn.org/stable/tutorial/machine_learning_map/#choosing-the-right-estimator

# https://scikit-learn.org/stable/

##############################
# Numpy 模組
##############################

import numpy as np

##############################
# 一維陣列
##############################

# 使用 tuple 或 list 建立一維陣列
a = np.array([1, 2, 3, 4, 5])
b = np.array((1, 2, 3, 4, 5), dtype=float) 

print(a)
print(b)

print(type(a))
print(type(b))

print(a[0], a[1], a[2], a[3])

b[0] = 5    
print(b) 

b[4] = 0
print(b)

##############################
# 二維陣列
##############################

# 使用巢狀清單建立二維陣列
# axis 0:列, axis 1:行
a = np.array([[1,2,3],[4,5,6]])
a 

print(type(a))

print(a[0, 0], a[0, 1], a[0, 2])

print(a[1, 0], a[1, 1], a[1, 2])

a[0, 0] = 6
a[1, 2] = 1
print(a)

# np.arrange
a = np.arange(5) # [0 1 2 3 4]
print(a) 

b = np.arange(1, 11, 2) # 1<= x < 11
print(b) # [1 3 5 7 9]

# np.zeros
np.zeros(5) # array([0., 0., 0., 0., 0.])

np.zeros(5, dtype=int) # array([0, 0, 0, 0, 0])

np.zeros((3, 2)) # 建立3列,2行皆為零的陣列
# array([[0., 0.],
#        [0., 0.],
#        [0., 0.]])

# np.ones 
np.ones(3) # array([1., 1., 1.])

# np.full
np.full(shape = (3, 4), fill_value = 99)
# array([[99, 99, 99, 99],
#        [99, 99, 99, 99],
#        [99, 99, 99, 99]])

# zeros_like
a = np.array([[1,2,3], [4,5,6]])
a
# array([[1, 2, 3],
#        [4, 5, 6]])

np.zeros_like(a)
# [[0 0 0]
#  [0 0 0]]

# ones_like
np.ones_like(a)
# [[1 1 1]
#  [1 1 1]]

##############################
# 陣列儲存與載入
##############################

# 實作練習
# 使用 save 將 Numpy 陣列 a 儲存成外部檔案

outputfile = 'myarray.npy'
with open(outputfile, 'wb') as fp:
    np.save(fp, a)

# 使用 load 將外部檔案匯入至Numpy陣列
import numpy as np
outputfile = "myarray.npy"
with open(outputfile, 'rb') as fp:
    mydata = np.load(fp)
print(mydata)

##############################
# 常數 Constants
##############################

import numpy as np

np.Inf # 無限大 inf

np.NAN # nan

# 新版本使用 nan
np.nan

np.pi # 3.141592653589793

# Euler’s constant, base of natural logarithms
# Napier’s constant(蘇格蘭數學家約翰·納皮爾)
np.e # 2.718281828459045

# 三角函數
# sin(30度) = sin(pi/6) = 0.5
# sin(45度) = sqrt(2)/2 = 0.707
# sin(60度) = sqrt(3)/2 = 0.866
# sin(90度) = 1
a = np.array([30, 45, 60, 90])
np.sin(a*np.pi/180)

##############################
# 亂數
##############################

import numpy as np

np.random.seed(123) # 設定亂數種子, 須輸入 >= 1 的整數

# random 產生0.0~1.0之間的1個亂數
x1 = np.random.random()
print(x1)

# random 產生0.0~1.0之間的3個亂數
x2 = np.random.random(3)
print(x2)

# rand 產生0.0~1.0之間的1個亂數
x3 = np.random.rand()
print(x3)

# rand 產生0.0~1.0之間的3個亂數
x4 = np.random.rand(3)
print(x4)

# rand(row, column) 產生亂數值陣列
x5 = np.random.rand(3, 2) # 3列,2行
print(x5)

# randint 產生 min 與 max 之間的整數亂數,不包括max
# randint(max, size)

# 建立 5~10之間的1個整數亂數
x6 = np.random.randint(5, 10)
print(x6)

# randint(min, max, size), min <= x < max

# 建立 1~11之間的10個整數亂數
x7 = np.random.randint(1, 11, size=10)
print(x7)

# 建立 1~11之間的4列5行陣列的整數亂數
x8 = np.random.randint(1, 11, size=(4, 5))
print(x8)

# 標準常態分配隨機樣本
# https://numpy.org/doc/stable/reference/random/generator.html

from numpy import random

# 舊版用法
vals = random.standard_normal(3)
print(vals)

more_vals = random.standard_normal(3)
print(more_vals)

# 新版用法
from numpy.random import default_rng

rng = default_rng()
vals = rng.standard_normal(3)
print(vals)

more_vals = rng.standard_normal(3)
print(more_vals)

##############################
# 陣列的屬性
##############################
import numpy as np

a = np.array([0,1,2,3,4,5])
a
a.dtype    # dtype('int32')
a.size     # 6
a.ndim     # 1
a.shape    # (6,)
a.itemsize # 4 bytes
a.nbytes   # 24

b = np.array([[1,2,3,4], [4,5,6,7], [7,8,9,10.]])
b
b.dtype    # float64
b.size     # 12
b.ndim     # 2
b.shape    # (3, 4)
b.itemsize # 8
b.nbytes   # 12*8=96

# 資料型別轉換
b.astype('int32')
b = b.astype('int32')
b.dtype    # int32

# 實作練習
# 建立3維陣列 myzero, 5個元素, 每個元素為3列,4行的零矩陣
# myzero.shape 結果為 (5, 3, 4)

##############################
# array 一維陣列 - loop 處理
##############################

a = np.array([1,2,3,4])
a

# 方法1. array - 取出元素, 使用for
for x in a:
  print(x)

# 方法2. array - 取出元素, 使用while
i = 0
while i < len(a):
  print(a[i])
  i = i + 1

# 方法3. array - 取出元素, 使用指標 range, len
for i in range(len(a)):
  print(a[i])

# 方法4. array - 取出元素, 使用陣列包含法
[print(x) for x in a]

##############################
# array 二維陣列 - loop 處理
##############################

a = np.array([[1,2,3,4], [5,6,7,8]])
a

for x in a:
  print(x)

for x in a:
    for item in x:
        print(str(item) + " ", end = " ")

##############################
# 陣列運算
##############################

a = np.array([1,2,3])
b = np.array([4,5,6])
a+b # 加
a-b # 減
a*b # 乘
a/b # 除

# 矩陣相乘(dot)
a = np.array([[1,2],[3,4],[5,6]])
a
b = np.array([[1,2],[3,4]])
b
a.shape
b.shape
c = a.dot(b) # 矩陣相乘(dot)
c

np.transpose(c) # 矩陣轉置
c.T             # 矩陣轉置

# inv()：反矩陣,逆矩陣 (inverse matrix)
from numpy.linalg import inv

x = np.array([[1, 2], [3, 4]])

inv(x)
# array([[-2. ,  1. ],
#        [ 1.5, -0.5]])

# 單位矩陣 (Identity matrix)
x.dot(inv(x))
# array([[1.00000000e+00, 1.11022302e-16],
#        [0.00000000e+00, 1.00000000e+00]])

x.dot(inv(x)).round(1)
# array([[1., 0.],
#        [0., 1.]])

# 計算矩陣行列式值 (determinant)
np.linalg.det(x)
# -2.0000000000000004

# 計算方形矩陣的特徵值 (eigenvalue) 與特徵向量 (eigenvector)
np.linalg.eig(x)
# (array([-0.37228132,  5.37228132]),
#  array([[-0.82456484, -0.41597356],
#         [ 0.56576746, -0.90937671]]))

##############################
# reshape 應用
##############################
import numpy as np

z = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
z
z.reshape(-1) # -1: unknown dimension
# array([ 1,  2,  3, ..., 10, 11, 12])

z.reshape(-1,1) # row -1: unknown , column 1

z.reshape(-1, 2) # row -1: unknown , column 2

z.reshape(1,-1) # row 1 , column: unknown

z.reshape(2, -1) # row 2 , column: unknown

z.reshape(3, -1) # row 3 , column: unknown

z.reshape(-1, -1) # ERROR

##############################
# 建立副本
##############################

# 建立副本-使用等號
a = np.array([0,1,1,2,3,5])
b = a.reshape((3,2)) # b之修改會影響a
b

b.ndim   # 2
b.shape  # (3,2)

b[1][0] = 168
b
a # a物件已經更改, array([  0,   1, 168,   3,   4,   5])

# 建立副本-使用 copy
c = a.reshape((3,2)).copy()
c
c[0][0] = -999
c
a # a物件沒有更改

##############################
# 2.CRISP-DM標準流程與模型績效評估
##############################

# 步驟 1：商業理解
# 步驟 2：資料理解
# 步驟 3：資料準備
# 步驟 4：模式建立
# 步驟 5：評估與測試
# 步驟 6：佈署應用

# https://en.wikipedia.org/wiki/Cross-industry_standard_process_for_data_mining

# 類別模型績效指標
# http://rwepa.blogspot.com/2013/01/rocr-roc-curve.html

##############################
# 3.案例實作練習 (行銷案例應用)
##############################

# 線上交易銷售資料
# 下載網址：https://www.kaggle.com/vijayuv/onlineretail
# 下載檔名：OnlineRetail.csv.zip
# 檔案筆數：541909列
# 欄位數：8欄
# 解壓縮後檔名: OnlineRetail.csv
# 解壓縮後大小：43.4MB
# 參考資料：https://towardsdatascience.com/data-driven-growth-with-python-part-1-know-your-metrics-812781e66a5b

# 直接 github 下載
# https://github.com/rwepa/DataDemo/blob/master/OnlineRetail.csv.zip

##############################
# 安裝與載入套件
##############################
# conda install plotly

# 載入套件
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from plotly.offline import plot # method 1: plot(fig)
import plotly.express as px # method 2: fig.show()
import plotly.graph_objs as go

# 設定 plotly繪圖預設顯示於瀏覽器
import plotly.io as pio
pio.renderers.default='browser'

# 匯入CSV檔案
df = pd.read_csv('OnlineRetail.csv') # ERROR for ANSI
df = pd.read_csv('OnlineRetail-utf8.csv') # OK for UTF-8

df

# 前5筆資料
df.head(5)

# 後5筆資料
df.tail()

# 欄位名稱
df.columns

# 資料摘要-數值
df.describe()

# 資料摘要-類別
# Country 唯一值為38個
df.describe(include = 'object')

# 資料摘要-數值＋類別
df.describe(include = 'all')

df.info()

# 找出有用的特徵(有意義的屬性)
# Customer ID
# Unit Price
# Quantity
# Invoice Date

# 將 InvoiceDate 欄位轉換為日期
df['InvoiceDate'] = pd.to_datetime(df['InvoiceDate'])
df['InvoiceDate']

# 新增評估欄位 InvoiceYearMonth 發票年月
df['InvoiceYearMonth'] = df['InvoiceDate'].map(lambda date: 100*date.year + date.month)
df['InvoiceYearMonth']

##############################
# 新增評估欄位 Revenue 收入
##############################

df['Revenue'] = df['UnitPrice'] * df['Quantity']
df['Revenue']

# 建立年月為群組,收入小計資料
df_revenue = df.groupby(['InvoiceYearMonth'])['Revenue'].sum().reset_index()
df_revenue

# 每月收入統計圖
plot_data = [
    go.Scatter(
        x=df_revenue['InvoiceYearMonth'],
        y=df_revenue['Revenue'],
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖1.每月收入統計圖')
fig = go.Figure(data=plot_data, layout=plot_layout)

# method 1
plot(fig)

# method 2
fig.show()

# 每月收入向上增加趨勢.
# 2011年2,4月達到最低點.

##############################
# 每月收入變化百分比
##############################

# 使用 pct_change() 函數計算"每月收入百分比變化"

df_revenue['MonthlyGrowth'] = df_revenue['Revenue'].pct_change()
df_revenue
# (560000.260-748957.020)/748957.020 = -0.252293

# 每月收入百分比變化統計圖
plot_data = [
    go.Scatter(
        x=df_revenue.query("InvoiceYearMonth < 201112")['InvoiceYearMonth'],
        y=df_revenue.query("InvoiceYearMonth < 201112")['MonthlyGrowth'],
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖2.每月收入百分比變化統計圖'
    )

fig = go.Figure(data=plot_data, layout=plot_layout)

fig.show()

##############################
# pandas 資料處理技巧
##############################

# 1.groupby 群組小計
# 2.rename 欄位重新命名
# 3.sort_values 排序

# 38個國家, 先考慮國家資料筆數最多者 - United Kingdom
df_country = df.groupby(['Country']).size().reset_index()
df_country

# 欄位重新命名
df_country.rename(columns={0:'Count'}, inplace=True)

# 排序
df_country.sort_values(by=['Count'], ascending=False)

# 建立篩選 United Kingdom 資料集
df_uk = df.query("Country=='United Kingdom'").reset_index(drop=True)
df_uk # 495478 rows × 10 columns

##############################
# 建立每月活動客戶數統計表
##############################

# nunique: Count distinct observations over requested axis.
df_monthly_active = df_uk.groupby('InvoiceYearMonth')['CustomerID'].nunique().reset_index()
df_monthly_active

# 每月活動客戶數長條圖
plot_data = [
    go.Bar(
        x=df_monthly_active['InvoiceYearMonth'],
        y=df_monthly_active['CustomerID'],
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖3.每月活動客戶數長條圖'
    )

fig = go.Figure(data=plot_data, layout=plot_layout)

fig.show()
# 2011年4月客戶數從3月的 923降低至 817, 減少 (817-923)/923 = -11.5%

##############################
# 每月每筆訂單平均收入
##############################

df_monthly_order_avg = df_uk.groupby('InvoiceYearMonth')['Revenue'].mean().reset_index()
df_monthly_order_avg

# 每月每筆訂單平均收入長條圖
plot_data = [
    go.Bar(
        x=df_monthly_order_avg['InvoiceYearMonth'],
        y=df_monthly_order_avg['Revenue'],
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖4.每月每筆訂單平均收入長條圖'
    )
fig = go.Figure(data=plot_data, layout=plot_layout)
fig.show()

##############################
# 建立評估欄位: 新客戶, 舊客戶
##############################

# 建立第1次消費之客戶
# 以客戶編號為群組, 計算 Min{發票日期}
df_min_purchase = df_uk.groupby('CustomerID').InvoiceDate.min().reset_index()

# 修改欄位名稱
df_min_purchase.columns = ['CustomerID','MinPurchaseDate']
df_min_purchase

# 新增欄位 - 轉換日期格式: 西元年月
df_min_purchase['MinPurchaseYearMonth'] = df_min_purchase['MinPurchaseDate'].map(lambda date: 100*date.year + date.month)

df_min_purchase # 3950 rows × 3 columns

df_uk # 495478 rows × 10 columns

# 合併 {MinPurchaseDate, MinPurchaseYearMonth} 至 df_uk 之最右側

df_uk = pd.merge(df_uk, df_min_purchase, on='CustomerID')

df_uk # 361878 rows × 12 columns

# 建立評估欄位: 客戶型態 UserType, 預設值為 New
# 如果 InvoiceYearMonth 大於 MinPurchaseYearMonth,表示之前為現有客戶, 將 UserType 改為 Existing

df_uk['UserType'] = 'New'

df_uk.loc[df_uk['InvoiceYearMonth'] > df_uk['MinPurchaseYearMonth'], 'UserType'] = 'Existing'

# 新,舊客戶的每月收入小計

# 計算每月新舊客戶收入小計
df_user_type_revenue = df_uk.groupby(['InvoiceYearMonth','UserType'])['Revenue'].sum().reset_index()

# 篩選資料並刪除前,後資料
df_user_type_revenue = df_user_type_revenue.query("InvoiceYearMonth != 201012 and InvoiceYearMonth != 201112")
df_user_type_revenue

# 繪圖
plot_data = [
    go.Scatter(
        x=df_user_type_revenue.query("UserType == 'Existing'")['InvoiceYearMonth'],
        y=df_user_type_revenue.query("UserType == 'Existing'")['Revenue'],
        name = 'Existing'
    ),
    go.Scatter(
        x=df_user_type_revenue.query("UserType == 'New'")['InvoiceYearMonth'],
        y=df_user_type_revenue.query("UserType == 'New'")['Revenue'],
        name = 'New'
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖5.新,舊客戶的每月收入統計圖'
    )
fig = go.Figure(data=plot_data, layout=plot_layout)
fig.show()

##############################
# 新客戶比率 (New Customer Ratio)
##############################

# 建立每月新客戶資料並刪除 NA
# 本例使用 unique 函數

df_user_ratio = df_uk.query("UserType == 'New'").groupby(['InvoiceYearMonth'])['CustomerID'].nunique()/df_uk.query("UserType == 'Existing'").groupby(['InvoiceYearMonth'])['CustomerID'].nunique()

# 設定 index
df_user_ratio = df_user_ratio.reset_index()

# 刪除 NA
df_user_ratio = df_user_ratio.dropna()

df_user_ratio

# 繪圖
# 考慮2月為新客戶,因此比例較高.

plot_data = [
    go.Bar(
        x=df_user_ratio.query("InvoiceYearMonth>201101 and InvoiceYearMonth<201112")['InvoiceYearMonth'],
        y=df_user_ratio.query("InvoiceYearMonth>201101 and InvoiceYearMonth<201112")['CustomerID'],
    )
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖6.新客戶比率統計圖'
    )
fig = go.Figure(data=plot_data, layout=plot_layout)
fig.show()

##############################
# 每月客戶保留率 (Monthly Retention Rate)
##############################

# 每月客戶保留率=上個月以前保留的客戶/總活躍客戶數

# 計算活躍客戶數, 依據 {CustomerID, InvoiceYearMonth}為群組, 計算收入總計.

df_user_purchase = df_uk.groupby(['CustomerID','InvoiceYearMonth'])['Revenue'].sum().reset_index()
df_user_purchase

# 使用 crosstab (交叉表) 建立客戶保留矩陣, crosstab 預設使用"發生次數"

df_retention = pd.crosstab(df_user_purchase['CustomerID'], df_user_purchase['InvoiceYearMonth']).reset_index()

df_retention

# 建立字典物件, 記錄月份, 總活躍客戶數, 保留客戶數
months = df_retention.columns[2:]
retention_array = []

for i in range(len(months)-1):
    retention_data = {}
    selected_month = months[i+1]
    prev_month = months[i]
    retention_data['InvoiceYearMonth'] = int(selected_month)
    retention_data['TotalUserCount'] = df_retention[selected_month].sum()
    retention_data['RetainedUserCount'] = df_retention[(df_retention[selected_month]>0) & (df_retention[prev_month]>0)][selected_month].sum()
    retention_array.append(retention_data)
retention_array

# 轉換為資料框
df_retention = pd.DataFrame(retention_array)
df_retention

# 計算保留率
df_retention['RetentionRate'] = df_retention['RetainedUserCount']/df_retention['TotalUserCount']
df_retention

# 繪圖
plot_data = [
    go.Scatter(
        x=df_retention.query("InvoiceYearMonth<201112")['InvoiceYearMonth'],
        y=df_retention.query("InvoiceYearMonth<201112")['RetentionRate'],
        name="organic"
    )
    
]

plot_layout = go.Layout(
        xaxis={"type": "category"},
        title='圖7.每月客戶保留率統計圖'
    )
fig = go.Figure(data=plot_data, layout=plot_layout)
fig.show()

##############################
# 4.關聯規則
##############################

# The Apriori implementation - mlxtend package
# https://github.com/rasbt/mlxtend

# reference:
# https://github.com/rwepa/DataDemo/blob/master/dm_chapter5_miningFrequentPatterns.pdf
# p.20 Example 5.3

# reference:
# Jiawei Han, Micheline Kamber, Jian Pei, Data Mining: Concepts and Techniques, Third Edition, 2012.
# http://myweb.sabanciuniv.edu/rdehkharghani/files/2016/02/The-Morgan-Kaufmann-Series-in-Data-Management-Systems-Jiawei-Han-Micheline-Kamber-Jian-Pei-Data-Mining.-Concepts-and-Techniques-3rd-Edition-Morgan-Kaufmann-2011.pdf

##############################
# 安裝 mlxtend
##############################

# method 1
# pip install mlxtend 

# method 2
# download and install
# https://pypi.python.org/pypi/mlxtend
# python setup.py install

# method 3 [採用此方法]
# conda config --add channels conda-forge
# conda install mlxtend

# method 4 [採用此方法] same as above.
# conda install -c conda-forge mlxtend

conda list mlxtend # v 0.21.0

import pandas as pd
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules

df = pd.read_csv('OnlineRetail-utf8.csv')
df.head()
df.describe()
print(df.dtypes)

##############################
# 資料清理
##############################

# 預設移除頭尾空白字元,eg.line 15: "IVORY KNITTED MUG COSY "
df['Description'] = df['Description'].str.strip()

# 移除InvoiceNo有NA
df.dropna(axis=0, subset=['InvoiceNo'], inplace=True)

df['InvoiceNo'] = df['InvoiceNo'].astype('str')

# 移除InvoiceNo有 C開頭資料 (credit transactions), 532621*8
df = df[~df['InvoiceNo'].str.contains('C')] 
df

# 交易資料合併,本例主要以法國為基準
basket = (df[df['Country'] =="France"]
          .groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo')) # 392*1563

# 獨熱編碼 (one hot encoding)
def encode_units(x):
    if x <= 0:
        return 0
    if x >= 1:
        return 1

basket_sets = basket.applymap(encode_units) # 編碼為0,1

basket_sets.drop('POSTAGE', inplace=True, axis=1) # 移除 POSTAGE 392*1562
basket_sets

basket_sets.info() # 0, 1 資料型態為 int64

# 建立頻繁項目集 (Frequent itemsets)
frequent_itemsets = apriori(basket_sets, min_support=0.07, use_colnames=True)

# C:\Users\asus\anaconda3\lib\site-packages\mlxtend\frequent_patterns\fpcommon.py:111: DeprecationWarning: DataFrames with non-bool types result in worse computationalperformance and their support might be discontinued in the future.Please use a DataFrame with bool type
# 資料型態改為 bool

# 選取數值欄位
column_names = basket_sets.select_dtypes(include=[np.number]).columns

# 將數值欄位轉換為 bool
basket_sets[column_names] = basket_sets[column_names].astype(bool)

# 再次執行 apriori, 結果已經沒有 DeprecationWarning
frequent_itemsets = apriori(basket_sets, min_support=0.07, use_colnames=True)

# 新增 頻繁項目集個數
frequent_itemsets['numbers'] = frequent_itemsets['itemsets'].apply(lambda x: len(x))

# generate the rules
# metric 預設值 = "confidence"
# min_threshold 預設值 = 0.8

rules = association_rules(frequent_itemsets, metric="lift", min_threshold=1)

rules

print(rules.to_string())

rules["antecedent_len"] = rules["antecedents"].apply(lambda x: len(x))
rules

rules[(rules['antecedent_len'] >= 2) &
      (rules['confidence'] > 0.9) &
      (rules['lift'] > 6)]

# http://rasbt.github.io/mlxtend/user_guide/frequent_patterns/association_rules/

# antecedents        : 先導(left-hand-side, LHS)
# consequents        : 後繼(right-hand-side, RHS)
# antecedent support : 先導支持度
# consequent support : 後繼支持度
# support            : 支持度(Support) = 購買A產品次數 / 全部交易數量
# confidence         : 信賴度(Confidence) = 購買A產品並同時購買B產品的次數 / 購買A產品次數
# lift               : 增益值
# leverage           : 槓桿值, 槓桿值為0表示獨立
# conviction         : 信念值: A high conviction value means that the consequent is highly depending on the antecedent.

##############################
# 5.PCA與集群分析
##############################

##############################
# 主成分分析 Principal Component Analysis, PCA
##############################

# find covariance matrix, eigenvalue and eigenvector

# Covariance is a measure of how changes in one variable are associated with changes in a second variable. Specifically, it’s a measure of the degree to which two variables are linearly associated.

import numpy as np

x1 = [90, 88, 60, 73, 82, 69, 77, 88, 59, 72]
x2 = [280, 261, 183, 277, 230, 234, 263, 278, 197, 230]

data = np.array([x1, x2])
data

cov_matrix = np.cov(data)
cov_matrix
# array([[ 126.62222222,  316.84444444],
#        [ 316.84444444, 1178.67777778]])

# x1 variance : 126.62
# x2 variance : 1178.68

eigen_vals, eigen_vecs = np.linalg.eig(cov_matrix)

print("Eigenvalues: ", eigen_vals)
# lambda_1 = 1266.73112247, lambda_2 = 38.56887753

print("Eigenvectors as columns: ", eigen_vecs)

##############################
# PCA demo - iris
##############################

# 載入模組
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt

# 載入 iris 資料集
iris = load_iris()
iris

# 區分特徵與反應變數
iris_X, iris_y = iris.data, iris.target

# 特徵名稱 (X)
iris.feature_names

# 反應變數(Y)的三種水準
iris.target_names

# 因子化反應變數 {0: 'setosa', 1: 'versicolor', 2: 'virginica'}
label_dict = {i: k for i, k in enumerate(iris.target_names)}

# 繪製散佈圖
def plot(X, y, title, x_label, y_label):
    for label,marker,color in zip(range(3), 
                                  ('^', 's', 'o'), 
                                  ('blue', 'red', 'green')):
        plt.scatter(x=X[:,0].real[y == label],
                    y=X[:,1].real[y == label],
                    color=color,
                    alpha=0.5,
                    label=label_dict[label])
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        leg = plt.legend(loc='upper right')
        leg.get_frame().set_alpha(0.5)
        plt.title(title)
    
plot(iris_X, iris_y, "Original Iris Data", "sepal length (cm)", "sepal width (cm)")

##############################
# 步驟1 共變異數矩陣計算 (numpy模組)
##############################

# 建立共變異數矩陣 (covariance matrix) - 使用 np.cov

import numpy as np

# 計算各行平均值向量
mean_vector = iris_X.mean(axis=0)
print(mean_vector)

# 計算共變異數矩陣
# var(x) = sum(x[i] - x_bar)^2)/(n-1)
# cov(x,y) = sum((x[i] - x_bar)*(y[i] - y_bar))/(n-1)

cov_mat = np.cov((iris_X).T)
cov_mat
cov_mat.shape # 4*4

##############################
# 步驟2 共變異數矩陣的特徵值計算
##############################

# 計算共變異數矩陣的特徵值 (eigenvalues)與特徵向量
# https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors

# 考慮非零向量 x, 如果 Ax = λx, 則稱 x 為 A 的特徵向量, λ 為 A 的特徵值.
# Ax - λIx = 0, 其中 I 為單位矩陣
# (A - λI)x = 0
# (A - λI) = 0, 即 (A - λI) 必為奇異矩陣 (singular matrix).
# det(A - λI) = 0, 可以解出 λ, 並進而解出 A

eig_val_cov, eig_vec_cov = np.linalg.eig(cov_mat)

# 特徵值, 計算結果為4個
eig_val_cov

# 特徵向量
eig_vec_cov

# 依遞減排序顯示
for i in range(len(eig_val_cov)):
    eigvec_cov = eig_vec_cov[:,i]
    print('特徵向量 {}: \n{}'.format(i+1, eigvec_cov))
    print('特徵值 {} Covariance matrix: {}'.format(i+1, eig_val_cov[i]))
    print(30 * '-')

##############################
# 步驟3 保留前 k 個特徵值- scree plot
##############################

# 使用陡坡圖 (碎石圖) Scree plot 決定保留 k 個特徵值
# 保留 k 個特徵值, 即是決定主成分個數, 所有主成分解釋100%資料的總變異數.
# 主成分(Principal Component)解釋變異數百分比 = "將每個主成分的特徵值"除以"所有特徵值的總和"
explained_variance_ratio = eig_val_cov/eig_val_cov.sum()

explained_variance_ratio
# array([0.92461872, 0.05306648, 0.01710261, 0.00521218])

# 累計主成分解釋變異數百分比
np.cumsum(explained_variance_ratio)

# 陡坡圖
plt.plot(np.cumsum(explained_variance_ratio), 
         linestyle='--', 
         marker='o', 
         color='b')
# plt.plot(np.cumsum(explained_variance_ratio), '--bo') # 與上面相同
plt.title('iris - Scree Plot')
plt.xlabel('Principal Component (k)')
plt.ylabel('% of Variance Explained <= k')

# 結論: 使用前2個主成分已經解釋原始資料變異數的98%, 達成維度縮減之目的.

##############################
# 步驟4 投影至新的主成分
##############################

# 保留前2個主成分, 選出前2行主成分 4*2
top_2_eigenvectors = eig_vec_cov[:,:2].T

# 顯示前2個特徵向量
top_2_eigenvectors

# iris 150*4 * pca_top2 4*2 = newdata 150*2
iris_pca = np.dot(iris_X, top_2_eigenvectors.T)

# 顯示前5列資料
iris_pca[:5,]

# 使用 scikit-learn 模組

from sklearn.decomposition import PCA

pca = PCA(n_components=2)

pca.fit(iris_X)

dir(pca)

top_2_eigenvectors.T # 人工計算
pca.components_ # scikit-learn計算

# 使用 transform 轉換至新的主成分
# 注意: transform 會執行中心化 (x - x_bar)
iris_pca_new = pca.transform(iris_X)

# 手工調整中心化
iris_pca_manual = np.dot(iris_X- mean_vector, top_2_eigenvectors.T)

iris_pca_new[:5,]

iris_pca_manual[:5,]

# 繪製PCA結果
plot(pca.transform(iris_X), 
     iris_y, 
     "Iris: Data projected onto first two PCA components", "PCA1", "PCA2")
# 有發現什麼改變?

##############################
# PCA demo - pokemon
##############################

import matplotlib.pyplot as plt

import pandas as pd

from sklearn.preprocessing import StandardScaler

from sklearn.decomposition import PCA

import numpy as np

df = pd.read_csv('pokemon.csv')
df.head(3)

# 資料標準化
cols = ['HP', 'Attack', 'Defense', 'SpecialAtk', 'SpecialDef', 'Speed']

scaler = StandardScaler().fit(df[cols])

# z = (x-平均值)/標準差
cols_std = scaler.transform(df[cols])

df_std = pd.DataFrame(cols_std, columns=cols)

df_std.describe()

# 考慮 PCA 主成分個數為2進行 PCA
num_pc = 2
pca = PCA(n_components=num_pc)
pca.fit(df_std)

loadings = pd.DataFrame(pca.components_, columns=cols)
loadings.index = ['PC'+str(i+1) for i in range(num_pc)]
loadings

# PCA 繪圖
pc_scores = pd.DataFrame(pca.transform(df_std)) # 轉換至PC1, PC2
pc_scores.columns = ['PC'+str(i+1) for i in range(num_pc)]
pc_scores.plot(kind='scatter', x='PC1', y='PC2')

# 考慮 PCA 主成分個數為6進行 PCA
num_pc = 6
pca = PCA(n_components=num_pc)
pca.fit(df_std)

# 特徵值
print(pca.explained_variance_)

 # 解釋變異比例
print(pca.explained_variance_ratio_)
sum(pca.explained_variance_ratio_) # 1

# 考量解釋 80% 的變異
pca = PCA(n_components=0.8)
pca.fit(df_std)
print(pca.explained_variance_) 
print(pca.explained_variance_ratio_)
sum(pca.explained_variance_ratio_) # 89%, 有4個主成分

loadings = pd.DataFrame(pca.components_, columns=cols)
loadings.index = ['PC'+str(i+1) for i in range(pca.n_components_)]
loadings

# 繪製解釋變異比例圖
var = np.array(pca.explained_variance_ratio_)
cum_var = np.cumsum(var)

plt.bar(range(1, len(var)+1), var, alpha=0.7, align='center',
        label='Individual explained variance')
plt.step(range(1, len(cum_var)+1), cum_var, where='mid', color='k',
         label='Cumulative explained variance')
plt.ylabel('Explained variance ratio')
plt.xlabel('Principle components')
plt.legend()

##############################
# 集群分析 (Clustering analysis)
##############################

"""
# kmeans demo with R
library(animation)
kmeans.ani()
"""

##############################
# kmeans 演算法-案例1 iris
##############################

from sklearn import datasets, cluster, metrics

# 讀入鳶尾花資料
iris = datasets.load_iris()
iris_X = iris.data

# KMeans 演算法
kmeans_fit = cluster.KMeans(n_clusters = 3).fit(iris_X)

# 印出分群結果
cluster_labels = kmeans_fit.labels_

# 集群法建模結果
print(cluster_labels)

# 實際值
iris_y = iris.target
print(iris_y)

# 績效評估
silhouette_avg = metrics.silhouette_score(iris_X, cluster_labels)
print(silhouette_avg)

##############################
# kmeans 演算法-案例2 Pokemon
##############################

import matplotlib.pyplot as plt

import pandas as pd

from sklearn.preprocessing import StandardScaler

from sklearn.cluster import KMeans

df = pd.read_csv('pokemon.csv')

# 對寶可夢兩個屬性的 SpecialAtk, SpecialDef 進行分群
t1, t2 = 'Bug', 'Psychic'
df_clf = df[(df['Type1']==t1) | (df['Type1']==t2)]
df_clf = df_clf[['Type1','SpecialAtk', 'SpecialDef']]

# 過濾出兩個屬性
df_clf.reset_index(inplace=True)
idx_0 = [df_clf['Type1']==t1]
idx_1 = [df_clf['Type1']==t2]

# 標準化
X = df_clf[['SpecialAtk', 'SpecialDef']]
scaler = StandardScaler().fit(X)
X_std = scaler.transform(X)
print(X_std[:2, :])

# kmeans 集群法, n_clusters 參數須提供
km = KMeans(n_clusters=2, init='random')
y_pred = km.fit_predict(X_std)
y_pred

# 建立繪圖函數
def plt_scatter(X_std, y_pred, km):
    
    # 設定顏色
    c1, c2 = 'red', 'blue'
        
    plt.scatter(X_std[y_pred==0, 0], 
                X_std[y_pred==0, 1], 
                color=c1, edgecolor='k', s=60)
    plt.scatter(X_std[y_pred==1, 0], 
                X_std[y_pred==1, 1], 
                color=c2, edgecolor='k', s=60)
    
    if len(X_std[y_pred==0]) < len(X_std[y_pred==1]):
        c1, c2 = 'blue', 'red'
 
    # 分群錯誤標計：
    # marker= '^' - Bug
    # marker= 'X' - Psychi
    # https://github.com/rwepa/DataDemo/blob/master/iPAS-python-program.py#L1387       
    
    plt.scatter(X_std[idx_0[0], 0], 
                X_std[idx_0[0], 1], 
                color=c1, marker='^', alpha=.5, 
                s=10, label=t1)
    
    plt.scatter(X_std[idx_1[0], 0], 
                X_std[idx_1[0], 1], 
                color=c2, marker='X', alpha=.5, 
                s=10, label=t2)
    # 各群中心點
    plt.scatter(km.cluster_centers_[:, 0], 
                km.cluster_centers_[:, 1], 
                s=100, marker='D', c='yellow', 
                edgecolor='black', label='centroids')
    
    plt.xlabel('SpecialAtk')
    plt.ylabel('SpecialDef')
    plt.title('K-means clustering on the Pokemon with k=2')    
    plt.legend()

plt_scatter(X_std, y_pred, km)

##############################
# kmeans 演算法-案例3 手寫數字集
##############################

# 參考: https://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html

# 先將原始資料進行 PCA的建模, 再執行 cluster

from time import time
import numpy as np
import matplotlib.pyplot as plt

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

np.random.seed(42)

# 載入資料
digits = load_digits()

# 標準化 (x-平均值)/標準差
data = scale(digits.data)

n_samples, n_features = data.shape
n_digits = len(np.unique(digits.target))
labels = digits.target

sample_size = 300

print("n_digits: %d, \t n_samples %d, \t n_features %d" % (n_digits, n_samples, n_features))

print(80 * '_')
print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilhouette')

def bench_k_means(estimator, name, data):
    t0 = time()
    estimator.fit(data)
    print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
          % (name, (time() - t0), estimator.inertia_,
             metrics.homogeneity_score(labels, estimator.labels_),
             metrics.completeness_score(labels, estimator.labels_),
             metrics.v_measure_score(labels, estimator.labels_),
             metrics.adjusted_rand_score(labels, estimator.labels_),
             metrics.adjusted_mutual_info_score(labels,  estimator.labels_),
             metrics.silhouette_score(data, estimator.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size)))

bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10), name="k-means++", data=data)

bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10), name="random", data=data)

# in this case the seeding of the centers is deterministic, hence we run the
# kmeans algorithm only once with n_init=1
pca = PCA(n_components=n_digits).fit(data)
bench_k_means(KMeans(init=pca.components_, n_clusters=n_digits, n_init=1),
              name="PCA-based",
              data=data)
print(82 * '_')

# Visualize the results on PCA-reduced data

reduced_data = PCA(n_components=2).fit_transform(data)
kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
kmeans.fit(reduced_data)

# Step size of the mesh. Decrease to increase the quality of the VQ.
h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

# Plot the decision boundary. For that, we will assign a color to each
x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1)
plt.clf()
plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')

plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)

# Plot the centroids as a white X
centroids = kmeans.cluster_centers_
plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='w', zorder=10)
plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
          'Centroids are marked with white cross')
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.show()

##############################
# 集群法的評估 - 集群內距離平方和 (SSE)
##############################

# 對寶可夢兩個屬性的 SpecialAtk, SpecialDef 進行分群
lst_type = ['Fairy', 'Fighting', 'Steel', 'Ice']

# 篩選 Type1 = Fairy
df_clf = df[df['Type1']==lst_type[0]] # 18*12

for i in range(1, len(lst_type)):
    df_clf = df_clf.append(df[df['Type1']==lst_type[i]])
    
# FutureWarning: The frame.append method is deprecated and will be removed from pandas in a future version. Use pandas.concat instead.
    
# 直接換成 df_clf.concat
# AttributeError: 'DataFrame' object has no attribute 'concat'
for i in range(1, len(lst_type)):
    df_clf = df_clf.concat(df[df['Type1']==lst_type[i]])

# 正確版本, 102*12
for i in range(1, len(lst_type)):
    df_clf = pd.concat([df_clf, df[df['Type1']==lst_type[i]]], axis=0, ignore_index=True)

X = df_clf[['SpecialAtk', 'SpecialDef']]
scaler = StandardScaler().fit(X)
X_std = scaler.transform(X)
X.shape # 102*2

繪製 SSE
lst_dst = []

# 嘗試 10 個 k 值並記錄分群結果的 SSE
# UserWarning: KMeans is known to have a memory leak on Windows with MKL, when there are less chunks than available threads. You can avoid it by setting the environment variable OMP_NUM_THREADS=1.

for i in range(1, 11):
    km = KMeans(n_clusters=i)
    km.fit(X_std)
    lst_dst.append(km.inertia_)

plt.plot(range(1, 11), lst_dst, marker='o')
plt.xlabel('Number of clusters')
plt.ylabel('SSE (Distortion)')
plt.title('SSE plot on the Pokemon')
plt.grid(color = 'gray', linestyle = '--', linewidth = 0.5)

##############################
# 集群法的評估 - 側影係數
##############################

import numpy as np
from matplotlib import cm
from sklearn.metrics import silhouette_samples, silhouette_score

for n_clusters in [2, 4]:
    
    fig, ax = plt.subplots(1, 1, dpi=100)
    
    # 雖然輪廓係數範圍是[-1, 1]，但這裡只顯示[-0.2, 0.8]之間
    ax.set_xlim([-.2, .8])
    ax.set_ylim([0, len(X_std)+(n_clusters+1)*10])
    
    # 建立 k-means 模型並擬合數據
    km = KMeans(n_clusters=n_clusters, random_state=0)
    y_pred = km.fit_predict(X_std)
    
    # 取出分群結果的標籤
    labels = np.unique(y_pred)
    
    # 計算所有樣本的輪廓係數平均值
    silhouette_avg = silhouette_score(X_std, y_pred)
    print("n_clusters =", n_clusters, "，所有樣本的輪廓係數平均 =", silhouette_avg)
    
    # 計算每個樣本的輪廓係數
    silhouette = silhouette_samples(X_std, y_pred, metric='euclidean')
    
    y_lower = 10
    
    for i, c in enumerate(labels):
        c_silhouette = silhouette[y_pred == c]
        c_silhouette.sort()
        size_cluster_i = c_silhouette.shape[0]
        y_upper = y_lower + size_cluster_i
        # 產生顏色編號，並填入區間內
        color = cm.nipy_spectral(float(i)/n_clusters)
        ax.fill_betweenx(np.arange(y_lower, y_upper), 0, 
                         c_silhouette, facecolor=color, 
                         edgecolor=color, alpha=0.7)
        # 標示集群標籤
        ax.text(-0.05, y_lower+0.5*size_cluster_i, str(i))
        y_lower = y_upper + 10
        
    ax.set_xlabel("The silhouette coefficient values")
    ax.set_ylabel("Cluster label")
    
    ax.axvline(x=silhouette_avg, color="red", linestyle="--")
    ax.set_yticks([])
    ax.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8])
    
# 階層式分群 (Hierarchical clustering)
# https://scikit-learn.org/stable/auto_examples/cluster/plot_agglomerative_dendrogram.html#sphx-glr-auto-examples-cluster-plot-agglomerative-dendrogram-py

##############################
# 6.案例實作練習
##############################

# 人力資源資料集 -----
# https://github.com/rwepa/DataDemo/blob/master/human_resource.csv
# 參考: https://www.kaggle.com/code/nirajvermafcb/principal-component-analysis-explained/notebook

# last_evaluation       : 最近考核分數 0~1分
# number_project        : 每年平均專案個數
# average_montly_hours  : 每月平均工作小時
# time_spend_company    : 在公司時間
# Work_accident         : 工安意外, 1:有, 0:沒有
# satisfaction_level    : 工作滿意度
# left                  : 是否離職, 1:離職, 0:沒有 --> 反應變數
# promotion_last_5years : 近5年是否有升遷, 1:有, 0:沒有
# role                  : 服務單位
# salary                : 薪資別: high, median, low

# 問題1: 平均工作滿意度為何?
# 問題2: 平均離職比例為何?
# 問題3: 使用 matplotlib 模組, 依不同 role 群組， 繪製由大至小平均工作滿意度水平長條圖?
# 問題4: 使用 scikit-learn 模組, 練習 PCA 應用.

# 載入套件
import numpy as np # 反矩陣, 特徵值, 特徵向量
import pandas as pd # 資料匯入與處理
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

# 匯入資料
df = pd.read_csv("human_resource.csv")

# 欄位名稱轉換為 list
columns_names = df.columns.tolist()
print(columns_names)

df.shape # 14999*10

df_summary = df.describe() # 資料摘要
df_summary

# 問題1: 平均工作滿意度?
# answer: 61.28%

# 問題2: 平均離職比例?
# answer: 23.81

df.info() # 沒有遺漏值, 2個欄位為字元型態, 即類別型資料

df.head()

df_corr = df.corr()

# 相關係數視覺化, 以下三行一起選取並執行
plt.figure(figsize=(10,10))
sns.heatmap(df_corr, vmax=1, square=True, annot=True, cmap='cubehelix')
plt.title('Correlation between 8 fearures')

# role 不重複次數, 10個類別
df['role'].unique()
# ['sales', 'accounting', 'hr', 'technical', 'support', 'management',　'IT', 'product_mng', 'marketing', 'RandD']

# 類別型變數 --> 群組統計, 例: sum
role_sum = df.groupby('role').sum()
role_sum

# 類別型變數 --> 群組統計, 例: mean
role_mean = df.groupby('role').mean()
role_mean

# 問題3: 使用 matplotlib 模組, 依不同 role 群組， 繪製由大至小平均工作滿意度水平長條圖?

# matplotlib: Horizontal bar chart

import matplotlib.pyplot as plt
import numpy as np

plt.rcdefaults()
fig, ax = plt.subplots()

mydf = role_mean.loc[:,['satisfaction_level']]

mydf.sort_values(by=['satisfaction_level'], ascending = False, inplace=True)

x_pos = np.arange(len(mydf.index))

# 長條圖
plt.bar(x_pos, mydf['satisfaction_level'], tick_label=mydf.index, width=0.5)

# 水平長條圖
mydf.sort_values(by=['satisfaction_level'], ascending = True, inplace=True)

plt.barh(x_pos, mydf['satisfaction_level'], tick_label=mydf.index, height=0.5)
plt.xlabel('Satisfaction level', fontsize="20") # 設定 x 軸標題內容及大小
plt.ylabel('Role', fontsize="20") # 設定 y 軸標題標題內容及大小
plt.title('satisfaction level chart', fontsize="20") # 設定圖表標題內容及大小

# 問題4: 使用 scikit-learn 模組, 練習 PCA, Clustering 應用.
# refer to materials

##############################
# 7.迴歸分析
##############################

# 迴歸分析簡介
# https://github.com/rwepa/DataDemo/blob/master/regression_01.pdf

##############################
# 迴歸分析 - statsmodels 模組
##############################

# https://www.statsmodels.org/

import statsmodels.api as sm
import pandas
from patsy import dmatrices # 建立設計矩陣 (design matrices)

# Guerry資料集
# https://vincentarelbundock.github.io/Rdatasets/doc/HistData/Guerry.html

# Andre-Michel Guerry (1833) 是法國第一個系統地收集和分析有關犯罪、識字和自殺等社會數據的人，以期確定社會現象與變量之間的關係。

# Department 部門名稱
# Lottery    皇家彩票的人均投注額
# Literacy   讀寫百分比(識字能力)
# Wealth     人均個人財產稅的排名指數
# Region     法國地區('N'='North', 'S'='South', 'E'='East', 'W'='West', 'C'='Central', 科西嘉島編碼為 NA)

df = sm.datasets.get_rdataset("Guerry", "HistData").data # 86*23

# 選取重要變數
vars = ['Department', 'Lottery', 'Literacy', 'Wealth', 'Region']
df = df[vars] # 86*5
df
#       Department  Lottery  Literacy  Wealth Region
# 0            Ain       41        37      73      E
# 1          Aisne       38        51      22      N
# 2         Allier       66        13      61      C
# 3   Basses-Alpes       80        46      76      E
# 4   Hautes-Alpes       79        69      83      E
# ..           ...      ...       ...     ...    ...
# 81        Vienne       40        25      68      W
# 82  Haute-Vienne       55        13      67      C
# 83        Vosges       14        62      82      E
# 84         Yonne       51        47      30      C
# 85         Corse       83        49      37    NaN
# [86 rows x 5 columns]

# 資料摘要
df.describe()
#          Lottery   Literacy     Wealth
# count  86.000000  86.000000  86.000000
# mean   43.500000  39.255814  43.500000
# std    24.969982  17.364051  24.969982
# min     1.000000  12.000000   1.000000
# 25%    22.250000  25.000000  22.250000
# 50%    43.500000  38.000000  43.500000
# 75%    64.750000  51.750000  64.750000
# max    86.000000  74.000000  86.000000

# 包括類別型變數資料摘要
df.describe(include='all')
#              Department    Lottery   Literacy     Wealth Region
# count                86  86.000000  86.000000  86.000000     85
# unique               86        NaN        NaN        NaN      5
# top     Basses-Pyrenees        NaN        NaN        NaN      C
# freq                  1        NaN        NaN        NaN     17
# mean                NaN  43.500000  39.255814  43.500000    NaN
# std                 NaN  24.969982  17.364051  24.969982    NaN
# min                 NaN   1.000000  12.000000   1.000000    NaN
# 25%                 NaN  22.250000  25.000000  22.250000    NaN
# 50%                 NaN  43.500000  38.000000  43.500000    NaN
# 75%                 NaN  64.750000  51.750000  64.750000    NaN
# max                 NaN  86.000000  74.000000  86.000000    NaN

# 資料資訊
df.info()
# <class 'pandas.core.frame.DataFrame'>
# RangeIndex: 86 entries, 0 to 85
# Data columns (total 5 columns):
#  #   Column      Non-Null Count  Dtype 
# ---  ------      --------------  ----- 
#  0   Department  86 non-null     object
#  1   Lottery     86 non-null     int64 
#  2   Literacy    86 non-null     int64 
#  3   Wealth      86 non-null     int64 
#  4   Region      85 non-null     object
# dtypes: int64(3), object(2)
# memory usage: 3.5+ KB

# NA值
df.isnull().sum()
df = df.dropna() # 85*5

# 最後5筆資料
df[-5:]

# 目標: 理解識字率與皇家彩票的人均賭注等變數相關變數

# 建立設計矩陣 (design matrices)
# 將類別型變數轉換為
y, X = dmatrices('Lottery ~ Literacy + Wealth + Region', data=df, return_type='dataframe')
X
y

# 建立迴歸模型
mod = sm.OLS(y, X) # Describe model

res = mod.fit() # Fit model

print(res.summary()) # Summarize model

res.params

dir(res)

##############################
# 迴歸分析 - scikit-learn 模組
##############################

# 匯入模組
import numpy as np

# 方法1
from sklearn.datasets import load_boston
boston = load_boston()
# FutureWarning: Function load_boston is deprecated; `load_boston` is deprecated in 1.0 and will be removed in 1.2.

type(boston) # sklearn.utils.Bunch

boston.data.shape # 506*13 (只有X, 不包括Y)
boston.target # 只有Y

# 方法2-使用 pandas
import pandas as pd
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.data', header=None, sep='\s+')

df.columns = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']

df.shape # 506*14(包括Y)
df.head()

# 散佈圖矩陣
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style='whitegrid', context='notebook')
cols = ['LSTAT', 'INDUS', 'NOX', 'RM', 'MEDV']

sns.pairplot(df[cols], size=2.5)
# UserWarning: The `size` parameter has been renamed to `height`; please update your code.

# 正確繪圖
sns.pairplot(df[cols], height=2.5)

# 儲存為 png, 以下2行程式,一起選取並執行.
sns.pairplot(df[cols], height=2.5)
plt.savefig('scatter_plot_matrix.png', dpi=300)

# 相關係數分析
cm = np.corrcoef(df[cols].values.T)
sns.set(font_scale=1.5)
hm = sns.heatmap(cm, 
            cbar=True,
            annot=True, 
            square=True,
            fmt='.2f',
            annot_kws={'size': 15},
            yticklabels=cols,
            xticklabels=cols)

# 資料分析
print(boston.data.shape) # x: (506, 13)

# ['CRIM' 'ZN' 'INDUS' 'CHAS' 'NOX' 'RM' 'AGE' 'DIS' 'RAD' 'TAX' 'PTRATIO'  'B' 'LSTAT']
print(boston.feature_names)

print(np.max(boston.target), np.min(boston.target), np.mean(boston.target))

print(boston.DESCR)

print(boston.data[0]) # 顯示第1筆記錄

print(np.max(boston.data), np.min(boston.data), np.mean(boston.data))

# 資料預處理
# from sklearn.cross_validation import train_test_split(舊版)
from sklearn.model_selection import train_test_split

# 將資料隨機抽樣成訓練集與測試集
X_train, X_test, y_train, y_test = train_test_split(boston.data, boston.target, test_size=0.25, random_state=33)

# Normalize data 標準化資料
from sklearn.preprocessing import StandardScaler

scalerX = StandardScaler().fit(X_train) # fit: compute mean and sd

# StandardScaler().fit(y_train) # Warning
# y_train.ndim = 1

# 新版 reshape(-1,1).ndim = 2
scalery = StandardScaler().fit(y_train.reshape(-1,1))

X_train = scalerX.transform(X_train) # 執行標準化
y_train = scalery.transform(y_train.reshape(-1,1))
X_test = scalerX.transform(X_test)
y_test = scalery.transform(y_test.reshape(-1,1))

print(np.max(X_train), np.min(X_train), np.mean(X_train), np.max(y_train), np.min(y_train), np.mean(y_train))

# K折交叉驗證
# five-fold cross-validation and coefficient of determination

# from sklearn.cross_validation import * (舊版用法)

from sklearn.model_selection import KFold

from sklearn.model_selection import cross_val_score

def train_and_evaluate(clf, X_train, y_train):
    
    clf.fit(X_train, y_train)
    
    print("Coefficient of determination on training set:", clf.score(X_train, y_train))
    
    # create a k-fold croos validation iterator of k=5 folds
    cv = KFold(n_splits=5, shuffle=True,  random_state=33)
    
    scores = cross_val_score(clf, X_train, y_train, cv=cv)
    
    print("Average coefficient of determination using 5-fold crossvalidation:",np.mean(scores))

# 線性模型 Linear model
from sklearn import linear_model

# 隨機梯度遞減法 SGD (Stochastic Gradient Descent)

# 梯度遞減法, Cauchy, 1847
# x[t+1] = x[t] - γ*▽(f(x[t])), γ: learning rate, ▽ 梯度,表示一階微分
# https://en.wikipedia.org/wiki/Gradient_descent

# 隨機梯度遞減法 Stochastic gradient descent
# SGD randomly picks one data point from the whole data set at each iteration to reduce the computations enormously.

# 最小批量梯度遞減法  Mini-batch gradient descent
# It is also common to sample a small number of data points instead of just one point at each step and that is called “mini-batch” gradient descent.

clf_sgd = linear_model.SGDRegressor(loss='squared_loss', penalty=None,  random_state=33)

train_and_evaluate(clf_sgd, X_train, y_train) # DataConversionWarning 有警告訊息

train_and_evaluate(clf_sgd, X_train, y_train.ravel())
print(clf_sgd.coef_)
print(clf_sgd.coef_[0])

# penalty with L2 norm (the squared sums of the coefficients)
clf_sgd1 = linear_model.SGDRegressor(loss='squared_loss', penalty='l2',  random_state=33)
train_and_evaluate(clf_sgd1, X_train, y_train.ravel())

##############################
# 迴歸分析視覺化
##############################

import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

x, y = df.loc[:, ['RM']], df.loc[:, ['MEDV']]
lr = LinearRegression()
lr.fit(x, y)
print('w_1 =', lr.coef_[0])
print('w_0 =', lr.intercept_)

# 繪製散佈圖與迴歸線 - matplotlib
plt.scatter(x, y, facecolor='xkcd:azure', edgecolor='black', s=20)
plt.xlabel('RM', fontsize=14)
plt.ylabel("MEDV", fontsize=14)

# 繪製迴歸線與迴歸線 - seaborn (95%信賴區間)
n_x = np.linspace(x.min(), x.max(), 100)
n_y = lr.intercept_ + lr.coef_[0] * n_x
plt.plot(n_x, n_y, color='r', lw=2)

# 繪製散佈圖 - seaborn
import seaborn as sns

# 預設會在迴歸線旁繪製 95% 的信賴區間
sns.regplot(x='RM', y='MEDV', 
            data=df, 
            scatter_kws={'facecolor':'xkcd:azure', 
                         'edgecolor':'black', 's':20},
            line_kws={'color':'r', 'lw':2})


##############################
# 邏輯斯迴歸 Logistic regression
##############################

##############################
# sigmoid function
##############################

import numpy as np
import matplotlib.pyplot as plt

def sigmoid(z):
    return 1./(1.+np.exp(-z))

z = np.linspace(-7, 7, 100)

plt.plot(z, sigmoid(z))
plt.axvline(0., color='black', ls='--', lw=2)
plt.xlabel('$z$')
plt.ylabel('sigmoid($z$)')
plt.ylim(-0.1, 1.1)
plt.yticks([0., 0.5, 1.])
plt.title("Sigmoid function", fontsize=20)

##############################
# 邏輯斯迴歸 - adult
##############################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('adult.csv')
print('數據大小：', df.shape) # 48842*15
print('遺漏值數量：', df.isnull().sum().sum())

【注意】:部分資料以?表示為遺漏值.
df.replace('?', np.nan, inplace=True)
print('遺漏值數量：', df.isnull().sum().sum()) # 6465

# 刪除 nan
df.dropna(inplace=True)
df.shape # 45222*15

df.head()

# 類別變數次數統計表
df['income'].value_counts()

# 將 <=50K 改為0, >50K 改為1
df['income'] = df['income'].map({'<=50K':0, '>50K':1})

# 篩選數值變數
features = ['age', 'fnlwgt', 'educational-num', 'capital-gain', 'capital-loss', 'hours-per-week']
df[features].describe()

# 篩選字串變數
df.describe(include=["O"])

# 年齡摘要統計
df['age'].describe()

# 年齡直方圖, 發現什麼?
df['age'].hist(grid=False)

# 標示高齡(大於70歲)人口數
plt.vlines(x=70, ymin=0, ymax=8500, colors='blue', linestyles='dotted')
plt.annotate('', xy=(70, 1600), 
             xycoords='data', 
             xytext=(85, 1600), textcoords='data', fontsize=16, 
             arrowprops=dict(arrowstyle='<-', color='black', connectionstyle="arc3"))
plt.text(68, 3000, '70', fontsize=12)
plt.text(72, 1800, '{}'.format(df[df["age"]>=70].shape[0]))

# 每週工時直方圖, 發現什麼?
df['hours-per-week'].hist()

# 使用 seaborn, 教育程度次數統計圖, 找出前3名
import seaborn as sns

plt.figure(figsize=(30,20))
total = float(len(df["income"]))

ax = sns.countplot(x="education", data=df)
for p in ax.patches:
    height = p.get_height()
    ax.text(p.get_x() + p.get_width()/2.,
            height + 3,
            '{:1.2f}'.format((height/total)*100),
            ha="center") 

# 依收入,性別為群組,繪製每週工時盒鬚圖
sns.boxplot(x='income', y ='hours-per-week', hue='gender', data=df)

# 相關係數視覺化
corr = df[features].corr()
sns.heatmap(corr, cmap='Blues', 
            annot=True, annot_kws={"size":10}, fmt='.1g', 
            square=True, linewidths=.5)

# 邏輯斯迴歸分類模型
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

X, y = df[features], df['income']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.2, random_state=0)

logit = LogisticRegression()
logit.fit(X_train, y_train)

# 測試集-模型準確度 Accuracy
logit.score(X_test, y_test)

# 混淆矩陣 plot_confusion_matrix 模組
from sklearn.metrics import plot_confusion_matrix

# 類別標籤
class_names = ['<=50K ', '>50K']

# 建立混淆矩陣 
disp = plot_confusion_matrix(logit, X_test, y_test, 
                             display_labels=class_names, 
                             cmap=plt.cm.Blues)
disp.ax_.set_title('Confuse Matrix on Adult dataset')

# 混淆矩陣-機率值
disp = plot_confusion_matrix(logit, X_test, y_test, 
                             display_labels=class_names, 
                             cmap=plt.cm.Blues,
                             normalize='true')
disp.ax_.set_title('Confuse Matrix on Adult dataset with probability')

# 產生分類報告 precision, recall, f1-score, support
from sklearn.metrics import classification_report
y_pred = logit.predict(X_test)
print(classification_report(y_test, y_pred))

# 使用 LogisticRegressionCV 模組
# This class implements logistic regression using liblinear, newton-cg, sag of lbfgs optimizer. 
from sklearn.linear_model import LogisticRegressionCV
from sklearn.preprocessing import StandardScaler

scalar = StandardScaler().fit(X_train)
X_train_std = scalar.transform(X_train)
X_test_std = scalar.transform(X_test)

# 預設從 1e-4 ~ 1e4 間產生 10 個 C 值進行交叉驗證
clf = LogisticRegressionCV(Cs=10, cv=5, penalty='l2', n_jobs=-1)
# n_jobs=-1 means using all processors

clf.fit(X_train_std, y_train)
print('最佳 C 值：', clf.C_)

y_pred = clf.predict(X_test_std)
print(classification_report(y_test, y_pred))

# 依預測機率調整輸出的類別
prob = logit.predict_proba(X_test)

# 設定機率門檻值，傾向預測為類別1
y_pred2 = [0 if p[0] > 0.6 else 1 for p in prob]
print(classification_report(y_test, y_pred2))

"""
補充篇: score
https://www.kaggle.com/getting-started/27261

A. predictor.score(X,Y) internally calculates Y'=predictor.predict(X) and then compares Y' against Y to give an accuracy measure. This applies not only to logistic regression but to any other model.

B. logreg.score(X_train,Y_train) is measuring the accuracy of the model against the training data. (How well the model explains the data it was trained with). <-- But note that this has nothing to do with test data.

C. logreg.score(X_test, Y_test) is equivalent to your print(classification_report(Y_test, Y_pred)). But you do not need to calculate Y_pred; that is done internally by the library
"""

##############################
# 8.決策樹與隨機森林法
##############################

# Information gain 資訊獲利, 目標為選取資訊獲利最大,即 Entropy(熵)較小者.
# Gini index 吉尼係數, 目標是選取吉尼係數較大者.
# https://chtseng.wordpress.com/2017/02/10/%E6%B1%BA%E7%AD%96%E6%A8%B9-decision-trees/

##############################
# 決策樹-鐵達尼號
##############################

# 匯入模組
# import sklearn as sk
import numpy as np

# 載入資料
# https://github.com/rwepa/DataDemo/blob/master/titanic.csv
import csv
with open('titanic.csv') as csvfile:
    titanic_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    
    # Header contains feature names
    # row = titanic_reader.next()
    # feature_names = np.array(row)
    feature_names = next(titanic_reader)
    
    # Load dataset, and target classes
    titanic_X, titanic_y = [], []
    for row in titanic_reader:
        titanic_X.append(row)
        titanic_y.append(row[2]) # The target value is "survived"
    titanic_X = np.array(titanic_X)
    titanic_y = np.array(titanic_y)

# 讀取欄位名稱
print(feature_names)

# 讀取第1筆資料
print(titanic_X[0], titanic_y[0])

# 資料預處理, 保留 class, age and sex
titanic_X = titanic_X[:, [1, 4, 10]]
feature_names = np.array(feature_names)[[1, 4, 10]]

print(feature_names)
print(titanic_X[12], titanic_y[12])

# 將 ages 為NA值以平均值填滿
ages = titanic_X[:, 1]
mean_age = np.mean(titanic_X[ages != 'NA', 1].astype(np.float))
titanic_X[titanic_X[:, 1] == 'NA', 1] = mean_age

# 將類別型資料編碼為數值型資料

# sex 編碼
from sklearn.preprocessing import LabelEncoder
enc = LabelEncoder()
label_encoder = enc.fit(titanic_X[:, 2])
# ['0' '1']
print("Categorical classes:", label_encoder.classes_)
integer_classes = label_encoder.transform(label_encoder.classes_)
print("Integer classes:", integer_classes)
t = label_encoder.transform(titanic_X[:, 2])
titanic_X[:, 2] = t

print(feature_names)
print(titanic_X[12], titanic_y[12])

# pclass 編碼
from sklearn.preprocessing import OneHotEncoder
enc = LabelEncoder()
label_encoder = enc.fit(titanic_X[:, 0])
print("Categorical classes:", label_encoder.classes_)

integer_classes = label_encoder.transform(label_encoder.classes_).reshape(3, 1)
print("Integer classes:", integer_classes)

enc = OneHotEncoder()
one_hot_encoder = enc.fit(integer_classes)

# First, convert clases to 0-(N-1) integers using label_encoder
num_of_rows = titanic_X.shape[0]
t = label_encoder.transform(titanic_X[:, 0]).reshape(num_of_rows, 1)

# Second, create a sparse matrix with three columns, each one indicating 
# if the instance belongs to the class
new_features = one_hot_encoder.transform(t)

# Add the new features to titanix_X
titanic_X = np.concatenate([titanic_X, new_features.toarray()], axis = 1)

# Eliminate converted columns
titanic_X = np.delete(titanic_X, [0], 1)

# Update feature names
feature_names = ['age', 'sex', 'first_class', 'second_class', 'third_class']

# Convert to numerical values
titanic_X = titanic_X.astype(float)
titanic_y = titanic_y.astype(float)

print(feature_names)
print(titanic_X[0], titanic_y[0])

# 訓練資料, 測試資料
# from sklearn.cross_validation import train_test_split (舊版本使用)
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(
    titanic_X, titanic_y, test_size=0.25, random_state=33)

# 訓練決策樹
from sklearn import tree
clf = tree.DecisionTreeClassifier(criterion='entropy', 
                                  max_depth=3, min_samples_leaf=5)
clf = clf.fit(X_train,y_train)
clf

# accuaacy for train data
from sklearn import metrics
def measure_performance(X,y,clf, show_accuracy=True,show_classification_report=True,show_confusion_matrix=True):
    y_pred=clf.predict(X)   
    if show_accuracy:
        print("Accuracy:{0:.3f}".format(metrics.accuracy_score(y,y_pred)),"\n")

    if show_classification_report:
        print("Classification report")
        print(metrics.classification_report(y,y_pred),"\n")
        
    if show_confusion_matrix:
        print("Confusion matrix")
        print(metrics.confusion_matrix(y,y_pred),"\n")
        
measure_performance(X_train,y_train,clf, show_classification_report=False, show_confusion_matrix=False)

# 繪製決策樹
tree.plot_tree(clf)

plt.figure(figsize=(18, 18), dpi = 300)  # set plot size (denoted in inches)
tree.plot_tree(clf, fontsize=6)

##############################
# 決策樹 - Pokemon
##############################

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

df = pd.read_csv('pokemon.csv')
df['hasType2'] = df['Type2'].notnull().astype(int)
X, y = df.loc[:, 'HP':'Speed'], df['hasType2']
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

# 建立決策樹分類器
clf = DecisionTreeClassifier(max_depth=3)
clf.fit(X_train, y_train)

# 產生分類報告
from sklearn.metrics import classification_report
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))

# 繪製決策樹
import matplotlib.pyplot as plt
from sklearn.tree import plot_tree

plt.figure(figsize=(12, 12), dpi = 100)
plot_tree(clf, filled=True, fontsize=10)
plt.savefig('pokemon_tree.png')

##############################
# 隨機森林法 - Pokemon
##############################

from sklearn.ensemble import RandomForestClassifier

clf = RandomForestClassifier(max_depth=3, n_jobs=-1)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))

# 重要特徵
importances = clf.feature_importances_
std = np.std([t.feature_importances_ for t in clf.estimators_], axis=0)
idx = np.argsort(importances)[::-1]

plt.title("Feature importances")
plt.bar(range(X.shape[1]), importances[idx])
plt.xticks(range(X.shape[1]), labels=X.columns[idx], fontsize=10)
plt.xlim([-1, X.shape[1]])
plt.ylim([0, 0.6])
plt.xticks(fontsize=10, rotation=45)

from sklearn.feature_selection import SelectFromModel

# 建立特徵選取器，門檻值預設為重要性的平均值
selector = SelectFromModel(clf)
selector.fit(X_train, y_train)
print('門檻值 =', selector.threshold_)
print('特徵遮罩：', selector.get_support())

# 選出新特徵，重新訓練隨機森林
X_train_new = selector.transform(X_train)
clf.fit(X_train_new, y_train)

X_test_new = selector.transform(X_test)
y_pred = clf.predict(X_test_new)
print(classification_report(y_test, y_pred))

##############################
# 9.案例實作練習
##############################

# 使用 scikit-learn 模組, 練習決策樹
# 使用 scikit-learn 模組, 練習決策樹
# refer to material

##############################
# 10.時間序列分析
##############################

# 時間序列 - 抗糖尿病藥物

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

# 匯入資料

url = 'https://raw.githubusercontent.com/rwepa/DataDemo/master/antidiabeticdrug.csv'

df = pd.read_csv(url, parse_dates=['date'], index_col='date')

df # 204*1
df.head()
df.tail()

# 時間序列繪圖
def plot_df(df, x, y, title='', xlabel='Date', ylabel='Sale', dpi=100):
    plt.figure(figsize=(16,5), dpi=dpi)
    plt.plot(x, y, color='tab:red')
    plt.gca().set(title=title, xlabel=xlabel, ylabel=ylabel)
    plt.show()

plot_df(df, x = df.index, y = df.sale, title='Monthly anti-diabetic drug sales in Australia from 1992 to 2008.')

# 建立新欄位
df['year'] = [d.year for d in df.index]
df['month'] = [d.strftime('%b') for d in df.index]
years = df['year'].unique() # 1991-2008

df.dtypes
# sale   float64
# year   int64
# month  object
# dtype: object

# 設定顏色
np.random.seed(100)

mycolors = np.random.choice(list(mpl.colors.XKCD_COLORS.keys()), len(years), replace=False)

# 群組繪圖
plt.figure(figsize=(16,12), dpi= 120)

for i, y in enumerate(years):
    if i > 0: # 考慮從1992年開始
        # i=1; y = 1992; plt.plot('month', 'sale', data=df.loc[df.year == y, :], color=mycolors[i], label = y)
        plt.plot('month', 'sale', data = df.loc[df.year == y, :], color=mycolors[i], label=y)
        plt.text(df.loc[df.year == y, :].shape[0] - 0.9, df.loc[df.year == y, 'sale'][-1:].values[0], y, fontsize=12, color=mycolors[i])

# Decoration
plt.gca().set(xlim=(-0.3, 11), ylim=(2, 30), ylabel='$Drug Sales$', xlabel='$Month$')
plt.yticks(fontsize=12, alpha=.7)
plt.title("Seasonal Plot of Drug Sales Time Series", fontsize=20)
plt.show()

# 盒鬚圖
fig, axes = plt.subplots(1, 2, figsize=(20,7), dpi= 120)
sns.boxplot(x='year', y='sale', data=df, ax=axes[0])
sns.boxplot(x='month', y='sale', data=df.loc[~df.year.isin([1991, 2008]), :])

# Set Title
axes[0].set_title('Year-wise Box Plot\n(The Trend)', fontsize=18); 
axes[1].set_title('Month-wise Box Plot\n(The Seasonality)', fontsize=18)

# 時間序列分解
from statsmodels.tsa.seasonal import seasonal_decompose

# 加法模型
result_add = seasonal_decompose(df['sale'], model='additive', extrapolate_trend='freq')
result_add.plot().suptitle('Additive Decompose', fontsize=12)

# 乘法模型
result_mul = seasonal_decompose(df['sale'], model='multiplicative', extrapolate_trend='freq')
result_mul.plot().suptitle('Multiplicative Decompose', fontsize=12)

# 時間序列分解
# Actual Values = Product of (Seasonal * Trend * Resid)

df_reconstructed = pd.concat([result_mul.seasonal, result_mul.trend, result_mul.resid, result_mul.observed], axis=1)
df_reconstructed.columns = ['seas', 'trend', 'resid', 'actual_values']
df_reconstructed.head()

# 時間序列穩態檢定(stationarity test)
from statsmodels.tsa.stattools import adfuller

# ADF Test
result = adfuller(df.sale, autolag='AIC')
print(f'ADF Statistic: {result[0]}') # f: float

print(f'p-value: {result[1]}') # p-value: 1.0

for key, value in result[4].items():
    print('Critial Values:')
    print(f'{key}, {value}')

# 繪製 ACF, PACF
import statsmodels.api as sm

# ACF and PACF upto 50 lags
sm.graphics.tsa.plot_acf(df.sale.tolist(), lags=50)
sm.graphics.tsa.plot_pacf(df.sale.tolist(), lags=50)

# 參考資料
# https://www.machinelearningplus.com/time-series/time-series-analysis-python/
# Forecasting: principles and practice 2nd Edition, 2018, https://otexts.com/fpp3/


##############################
# 11.類神經網路與深度學習
##############################

##############################
# 類神經網路
##############################

# 人工智慧1950 --> 機器學習1980 --> 深度學習2006

conda list tensorflow

##############################
# 啟動函數 (Activation function)
##############################

# Sigmoid 函數
import numpy as np
import matplotlib.pyplot as plt

def sigmoid(x):
    return 1/(1+(np.e**(-x)))

x = np.arange(-6, 6, 0.1)

plt.plot(x, sigmoid(x))
plt.title("Sigmoid function (0~1)")
plt.show()

# ReLU 函數
import numpy as np
import matplotlib.pyplot as plt

def relu(x):
    return np.maximum(0, x)

x = np.arange(-6, 6, 0.1)

plt.plot(x, relu(x))
plt.title("ReLU function, f(x)=max(0,x)")
plt.show()

# Tanh 函數
import numpy as np
import matplotlib.pyplot as plt

def tanh(x):
    return np.tanh(x)

x = np.arange(-6, 6, 0.1)

plt.plot(x, tanh(x))
plt.title("Tanh function (-1 ~ 1)")
plt.show()

# Softmax 函數
import numpy as np

def softmax(x):
    return np.exp(x)/sum(np.exp(x))

x = np.array([1,2,3,4,1,2,3])

y = softmax(x)
print(y)

##############################
# iris - 多層感知器
##############################

# 步驟1 載入套件
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical

# 步驟2 資料預處理
np.random.seed(7)  # 指定亂數種子

# 載入資料集
# https://github.com/rwepa/DataDemo/blob/master/iris.csv
# df = pd.read_csv("iris.csv")

urls = "https://raw.githubusercontent.com/rwepa/DataDemo/master/iris.csv"
df = pd.read_csv(urls)
df

# one-hot 編碼
target_mapping = {"setosa": 0,
                  "versicolor": 1,
                  "virginica": 2}

df["Species"] = df["Species"].map(target_mapping)

dataset = df.values # 取出資料框的值
np.random.shuffle(dataset)  # 使用亂數打亂資料的列順序

# 分割成特徵資料(X)和標籤資料(Y)
X = dataset[:,0:4].astype(float)
Y = to_categorical(dataset[:,4])

# 特徵標準化
X -= X.mean(axis=0)
X /= X.std(axis=0)

# 分割成訓練和測試資料集
X_train, Y_train = X[:120], Y[:120]     # 訓練資料前120筆
X_test, Y_test = X[120:], Y[120:]       # 測試資料後30筆

# 步驟3 定義模型
# 建立Keras的Sequential模型
# input(4)-->hiden1(6)-->hiden2(6)-->output(3)
# 輸入層 4個特徵
# 第1隱藏層 6個神經元
# 第2隱藏層 6個神經元
# 輸出層 3個神經元

model = Sequential() # 建立 Sequential 物件
model.add(Dense(6, input_shape=(4,), activation="relu"))
model.add(Dense(6, activation="relu"))
model.add(Dense(3, activation="softmax"))
model.summary()   # 顯示模型摘要

# dense (Dense)   : 4*6 + 6 = 30
# dense_1 (Dense) : 6*6 + 6 = 42
# dense_2 (Dense) : 6*3 + 3 = 21
# 合計 = 30 + 42 + 21 = 93

# 步驟4 編譯模型

# 編譯模型
# loss 損失函數, optimizer 優化器即梯度下降法, metrics 評估標準
# https://www.tensorflow.org/api_docs/python/tf/keras/optimizers

model.compile(loss="categorical_crossentropy", 
              optimizer="adam",
              metrics=["accuracy"])

# 步驟5 訓練模型
# 需一些時間

# epochs 訓練週期,  batch_size 批次樣本大小

print("Training ...")
model.fit(X_train, Y_train, epochs=100, batch_size=5)

# 步驟6 評估與儲存模型

# 評估模型
print("\nTesting ...")
loss, accuracy = model.evaluate(X_test, Y_test)
print("準確度 = {:.2f}".format(accuracy))

# 儲存 Keras 模型
print("Saving Model: iris.h5 ...")
model.save("iris.h5")

# 使用儲存模型進行預測
from tensorflow import keras

model = Sequential()
model = keras.models.load_model("iris.h5")
model.compile(loss="categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

loss, accuracy = model.evaluate(X_test, Y_test)
print("測試資料集的準確度 = {:.2f}".format(accuracy))

Y_pred = model.predict_classes(X_test)
print(Y_pred)

Y_target = dataset[:,4][120:].astype(int)
print(Y_target)

# 混淆矩陣 (Confusion Matrix)
cm = pd.crosstab(Y_target,
                 Y_pred,
                 rownames=["Actual"],
                 colnames=["Predict"])
print(cm)

##############################
# MLP + 時間序列
##############################

##############################
# MLP視覺化 VisualizeNN 程式
##############################

# 步驟1:安裝 palettable 套件: Color Palettes for Python
# conda install palettable

# 步驟2:下載 VisualizeNN.py
# https://github.com/jzliu-100/visualize-neural-network

import VisualizeNN as VisNN
network=VisNN.DrawNN([3,4,1])
network.draw()

##############################
# 資料處理-轉換為samples
##############################
# 時間序列 [10,20,30,40,50,60,70,80,90]
# X,
# 10,20,30  40
# 20,30,40  50
# 30,40,50  60
# 40,50,60  70
# 50,60,70  80
# 60,70,80  90
# 70,80,90  ?

from numpy import array

# 將資料轉換成 samples
def split_sequence(sequence, n_steps):
	X, y = list(), list()
	for i in range(len(sequence)):
		
        # find the end of this pattern
		end_ix = i + n_steps
		
        # check if we are beyond the sequence
		if end_ix > len(sequence)-1:
			break
		
        # gather input and output parts of the pattern
		seq_x, seq_y = sequence[i:end_ix], sequence[end_ix]
		X.append(seq_x)
		y.append(seq_y)
	return array(X), array(y)

# 建立時間序列
raw_seq = [10, 20, 30, 40, 50, 60, 70, 80, 90]

# 輸入: three time steps
# 輸出: one time step (one-step) 預測
n_steps = 3

# 將資料轉換為 samples
X, y = split_sequence(raw_seq, n_steps)
X
y

# summarize the data
for i in range(len(X)):
	print(X[i], y[i])

##############################
# 建立模型與預測
##############################

# MLP應用
from numpy import array
from keras.models import Sequential
from keras.layers import Dense

# 定義模型
model = Sequential()
model.add(Dense(100, activation='relu', input_dim=3))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mse')

# fit model
# verbose=0 will show you nothing (silent)
# verbose=1 will show you an animated progress bar
# verbose=2 will just mention the number of epoch
    
model.fit(X, y, epochs=2000, verbose=0)

# 預測
x_input = array([70, 80, 90])
x_input = x_input.reshape((1, 3))
yhat = model.predict(x_input, verbose=0)
print(yhat)

##############################
# 深度學習 CNN
##############################

# https://www.tensorflow.org/tutorials/images/cnn

# CIFAR10 資料集
# https://www.cs.toronto.edu/~kriz/cifar.html

# 圖片區分為10類: airplane, automobile, bird,...
# 每一類別有 6000張 圖片, 共 60000張圖片, 每張圖片大小 32*32

# 使用 Keras Sequential API
import tensorflow as tf

from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt

# 60,000 color images in 10 classes, with 6,000 images in each class. 
# The dataset is divided into 50,000 training images and 10,000 testing images. 

(train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

# 檢視資料大小
print(train_images.shape) # (50000, 32, 32, 3)
print(train_labels.shape) # (50000, 1)
print(test_images.shape) # (10000, 32, 32, 3)
print(test_labels.shape) # (10000, 1)

# 檢視第1張圖片, 3維陣列
tmp = train_images[0]
print(tmp)

print(train_labels[0]) # 6 --> frog

# 標準化為 0~1
train_images, test_images = train_images / 255.0, test_images / 255.0

# 繪圖
class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i])
    # The CIFAR labels happen to be arrays, 
    # which is why you need the extra index
    plt.xlabel(class_names[train_labels[i][0]])

# 建立 CNN 模型

# 輸入層      input(32, 32, 3)
# 第1次卷積層  Conv2D(32, 3, 3)
# 第1次池化層  MaxPooling2D (2,2)
# 第2次卷積層  Conv2D(64, 3, 3)
# 第2次池化層  MaxPooling2D (2,2)
# 第3次卷積層  Conv2D(64, 3, 3)

model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))

# 模型架構
model.summary()

# 建立平坦層, 將特徵值轉為一維資料以供後續的全連結層(隱藏層)使用
model.add(layers.Flatten())

# 建立隱藏層
model.add(layers.Dense(64, activation='relu'))

# 建立輸出層
model.add(layers.Dense(10))

# 模型摘要
model.summary()

# 編譯與訓練模型
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# 需要一些時間
history = model.fit(train_images, train_labels, epochs=10, validation_data=(test_images, test_labels))

# 評估模型(訓練集與驗證集準確率)
plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')

# 測試集準確度
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print(test_acc) # 70%

##############################
# 12.案例實作練習
##############################

# 圖像分類 (Image classification)
# https://www.tensorflow.org/tutorials/images/classification
# end
# 恭喜您, 開啟人生 Python 學習之旅 ^_^
