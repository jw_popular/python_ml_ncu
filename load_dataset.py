# 載入套件
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from plotly.offline import plot # method 1: plot(fig)
import plotly.express as px # method 2: fig.show()
import plotly.graph_objs as go

# 設定 plotly繪圖預設顯示於瀏覽器
import plotly.io as pio
pio.renderers.default='browser'

# 匯入CSV檔案
df = pd.read_csv('OnlineRetail_utf8.csv') # ERROR for ANSI
