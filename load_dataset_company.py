# 載入套件
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from plotly.offline import plot # method 1: plot(fig)
import plotly.express as px # method 2: fig.show()
import plotly.graph_objs as go

# 設定 plotly繪圖預設顯示於瀏覽器
import plotly.io as pio
pio.renderers.default='browser'

file1="data_for_dsph.cvs"
file2="data_for_dspk.cvs"
file3="oce_dsph_technisat.xls"

# 匯入CSV檔案
#df = pd.read_csv('OnlineRetail_utf8.csv') # ERROR for ANSI
#df = pd.read_csv(file1) # ERROR for ANSI
df = pd.read_excel(file3)

# 前5筆資料
print(df.head(5))

# 後5筆資料
print(df.tail())

# 欄位名稱
print("-----columns-----")
print(df.columns)
print("-----columns-----")

# 資料摘要-數值
print(df.describe())

# 資料摘要-類別
# Country 唯一值為38個
print(df.describe(include = 'object'))

# 資料摘要-數值＋類別
print(df.describe(include = 'all'))

print(df.info())

# 找出有用的特徵(有意義的屬性)
# Customer ID
# Unit Price
# Quantity
# Invoice Date

# 將 InvoiceDate 欄位轉換為日期
#df['InvoiceDate'] = pd.to_datetime(df['InvoiceDate'])
#print(df['InvoiceDate'])

# 新增評估欄位 InvoiceYearMonth 發票年月
#df['InvoiceYearMonth'] = df['InvoiceDate'].map(lambda date: 100*date.year + date.month)
#print(df['InvoiceYearMonth'])

