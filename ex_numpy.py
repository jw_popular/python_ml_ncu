##############################
# Numpy 模組
##############################

import numpy as np

##############################
# 一維陣列
##############################

# 使用 tuple 或 list 建立一維陣列
a = np.array([1, 2, 3, 4, 5])
b = np.array((1, 2, 3, 4, 5), dtype=float) 

print(a)
print(b)

print(type(a))
print(type(b))

print(a[0], a[1], a[2], a[3])

print(a[-1])

b[0] = 5    
print(b) 

b[4] = 0
print(b)